# 员工管理系统

#### Description
员工管理系统---大项目前的热身小项目
项目描述：
	1）服务器负责管理所有员工表单（以数据库形式或文件形式都可），其他客户端可通过网络连接服务器来查询员工表单。
	2）需要账号密码登陆，其中需要区分管理员账号还是普通用户账号。
	3）管理员账号可以查看、修改员工表单，管理员要负责管理所有的普通用户，且管理员账号只能在服务器端登陆。
	4）普通用户只能查询与本人有关的相关信息，其他员工信息（出于保密原则）不得泄露。
	5）有查询历史记录功能。
	6）能同时处理多台客户端的请求功能。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
